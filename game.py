import os

def _show_game_board(game_board, turn):
	os.system('clear')
	output = [
		'  1 2 3',
		'  _____',
		'a|%s*%s*%s|' % (game_board[0][0],
						 game_board[0][1],
						 game_board[0][2]),
		' |*****|',
		'b|%s*%s*%s|' % (game_board[1][0],
						 game_board[1][1],
						 game_board[1][2]),
		' |*****|',
		'c|%s*%s*%s|' % (game_board[2][0],
						 game_board[2][1],
						 game_board[2][2]),
		'  -----',
		' %s Player\'s Move' % ('First' if turn == 1 else 'Second',),
		' Or Enter q to go back to the menu'
	]

	for line in output:
	    print(line)

def _index(cmd):
	return [['a', 'b', 'c'].index(cmd[0]),
		['1', '2', '3' ].index(cmd[1])]

def _is_valid(cmd):
	if len(cmd) != 2:
		return False

	return (cmd[0] in ['a', 'b', 'c']) \
		and (cmd[1] in ['1', '2', '3'])

def _is_empty(game_board, cmd):
	x, y = _index(cmd)

	return game_board[x][y] == ' '

def _move(game_board, turn, cmd):
	x, y = _index(cmd)
    
	game_board[x][y] = 'X' if turn == 1 else 'O'

def _is_win(game_board):
	win_states = ['X' * 3, 'O' * 3]
	for i in range(3):
		vertical, horizontal = '', ''
		for j in range(3):
			horizontal += game_board[i][j]
			vertical += game_board[j][i]

		if (horizontal in win_states) or (vertical in win_states):
			return True

	diagonal_1, diagonal_2 = '', ''
	for i in range(3):
		diagonal_1 += game_board[i][i]
		diagonal_2 += game_board[i][2-i]

	return (diagonal_1 in win_states) or (diagonal_2 in win_states)

def _is_tie(game_board):
	for row in game_board:
		for item in row:
			if item == ' ':
				return False

	if not _is_win(game_board):
		return True
	else:
		return False

def init():
	game_board = []
	for i in range(3):
		game_board.append([' '] * 3)

	turn = 1

	while True:
		_show_game_board(game_board, turn)

		cmd = input('Enter Move: ').strip()
		if cmd == 'q':
			return 'exited from the game'

		if _is_valid(cmd):
			if _is_empty(game_board, cmd):
				_move(game_board, turn, cmd)
				turn *= -1

		if _is_win(game_board):
			return '%s Player Won' \
				% ('First' if turn == -1 else 'Second',)

		if _is_tie(game_board):
			return 'It was a Tie'
